define([
    'config',
    'models/data',
    'views/total-count',
    'views/users-in-time',
    'views/sections-count',
    'views/pages-count',
    'views/traffic-count'
], function (config, DataModel, TotalCount, UsersInTime, SectionsCount, PagesCount, TrafficCount) {
    var app = {
        model: undefined,
        totalCount: undefined,
        usersInTime: undefined,
        sectionsCount: undefined,
        pagesCount: undefined,
        trafficCount: undefined,

        initialize: function()  {
            this.model = new DataModel();
            this.totalCount = new TotalCount({
                model: this.model
            });
            this.usersInTime = new UsersInTime({
                model: this.model
            });
            this.sectionsCount = new SectionsCount({
                model: this.model
            });
            this.pagesCount = new PagesCount({
                model: this.model
            });
            this.trafficCount = new TrafficCount({
                model: this.model
            })
        }
    };

    return app;
});

