define([
    'utils/date',
    'text!templates/sections-count-list.html'
],function (date, listTemplate) {
    var SectionsCount = Backbone.View.extend({
        model: undefined,
        chartDOM: undefined,

        initialize: function(options) {
            this.model = options.model;
            this.chartDOM = $('#sections-count-list');
            this.listenTo(this.model, "change:data", this.onDataChange.bind(this))
        },
        render: function() {
            this.chartDOM.html(_.template(listTemplate, {
                sectionsCount: this.model.sectionsCount
            }));
            this.chartDOM.tableSort({
                animation: 'slide',
                speed: 500
            })
        },
        onDataChange: function() {
            this.render();
        }

    });
    return SectionsCount;
});