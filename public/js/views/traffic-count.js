define([
    'text!templates/traffic-count-list.html'
],function (listTemplate) {
    var SectionsCount = Backbone.View.extend({
        model: undefined,
        chartDOM: undefined,

        initialize: function(options) {
            this.model = options.model;
            this.chartDOM = $('#traffic-count-list');
            this.listenTo(this.model, "change:data", this.onDataChange.bind(this))
        },
        render: function() {
            this.chartDOM.html(_.template(listTemplate, {
                trafficCount: this.model.trafficCount
            }));
            //this.chartDOM.tableSort({
            //    animation: 'slide',
            //    speed: 500
            //})
        },
        onDataChange: function() {
            this.render();
        }

    });
    return SectionsCount;
});