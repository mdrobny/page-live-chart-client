define([
    'utils/date'
],function (date) {
    var usersInTime = Backbone.View.extend({
        model: undefined,
        chartDOM: undefined,
        chartContext: undefined,
        chart: undefined,
        data: undefined,

        initialize: function(options) {
            this.model = options.model;
            this.chartDOM = $('#users-in-time-chart');
            this.listenTo(this.model, "change:data", this.onDataChange.bind(this))
        },
        render: function() {

            this.chart = new Chartkick.LineChart('users-in-time-chart', this.model.usersInTime, {
                //min: new Date()
                discrete: "datetime"
            });
        },
        onDataChange: function() {
            this.render();
        }

    });
    return usersInTime;
});