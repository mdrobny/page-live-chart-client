define([
    'text!templates/total-count.html'
],function (totalCountTemplate) {
    var TotalCount = Backbone.View.extend({
        model: undefined,
        chartDOM: undefined,

        initialize: function(options) {
            this.model = options.model;
            this.chartDOM = $('#total-count');
            this.listenTo(this.model, "change:data", this.onDataChange.bind(this))
        },
        render: function() {
            this.chartDOM.html(_.template(totalCountTemplate, {
                totalCount: this.model.totalCount
            }));
        },
        onDataChange: function() {
            this.render();
        }

    });
    return TotalCount;
});