define([
    'utils/date',
    'text!templates/pages-count-list.html'
],function (date, listTemplate) {
    var PagesCount = Backbone.View.extend({
        model: undefined,
        chartDOM: undefined,

        initialize: function(options) {
            this.model = options.model;
            this.chartDOM = $('#pages-count-list');
            this.listenTo(this.model, "change:data", this.onDataChange.bind(this))
        },
        render: function() {
            this.chartDOM.html(_.template(listTemplate, {
                pagesCount: this.model.pagesCount,
                domain: window.domain
            }));
            //this.chartDOM.tableSort({
            //    animation: 'slide',
            //    speed: 500
            //})
        },
        onDataChange: function() {
            this.render();
        }

    });
    return PagesCount;
});