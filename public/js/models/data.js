define([
    'config',
    'utils/date'
], function (config, date) {
    var usersInTimeModel = Backbone.Model.extend({
        source: undefined,
        usersInTime: undefined,
        sectionsCount: undefined,
        pagesCount: undefined,
        totalCount: undefined,
        trafficCount: undefined,

        initialize: function() {
            if(typeof EventSource !== "undefined") {
                this.source = new EventSource(config.serverUrl + 'data/' + window.domain +'?auth=' + window.apiToken);
            }  else {
                console.warn('EventSource not supported..')
            }

            this.source.onopen = function (e) {
                console.log('Connection open');
            };

            this.source.onmessage = function(e) {
                if(e.origin === config.originUrl) {
                    //this.set(this._parseData(e.data));
                    this._parseData(e.data);
                    this.trigger('change:data')
                }  else {
                    console.warn('Wrong origin')
                }
            }.bind(this);
        },
        _parseData: function(data) {
            data = JSON.parse(data);

            this._parseTotalCount(data);
            this._parseUsersInTime(data);
            this._parseSectionsCount(data);
            this._parsePagesCount(data);
            this._parseTraffic(data);
        },
        _parseTotalCount: function(data) {
            var currentData = new Date();
            currentData = date.formatDateTime(currentData);
            this.totalCount = {
                count: data.count,
                loggedCount: data.loggedCount,
                currentData: currentData
            }
        },
        _parseUsersInTime: function(data) {
            var dateTime, length, keys;
            if(!this.usersInTime) {
                this.usersInTime = {};
            }
            dateTime = date.formatTime(new Date(data.date));

            this.usersInTime[dateTime] = data.count;

            keys = _.keys(this.usersInTime);
            length = keys.length;
            if(length > 15) {
                delete this.usersInTime[keys[0]];
            }
        },
        _parseSectionsCount: function(data) {
            if(!this.sectionsCount) {
                this.sectionsCount = {};
            }

            this.sectionsCount = _.clone(data.sections);
        },
        _parsePagesCount: function(data) {
            if(!this.pagesCount) {
                this.pagesCount = {};
            }

            this.pagesCount = _.clone(data.pages);
            //this.pagesCount = _.sortBy(this.pagesCount, 'count');
        },
        _parseTraffic: function(data) {
            if(!this.trafficCount) {
                this.trafficCount = {};
            }

            this.trafficCount = _.clone(data.traffic);
            //this.trafficCount = _.sortBy(this.trafficCount, 'count');
        }
    });
    return usersInTimeModel;
});