require.config({
    enforceDefine: true,
    paths: {
        'text': 'libs/requirejs-plugins/text',
        'templates': '../templates'
    }
});

define(['app'], function (app) {
    app.initialize();
});