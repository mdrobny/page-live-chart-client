/* global define */

define(['utils/number'], function (number) {
    var date = {};

    date.formatDate = function(date) {
        return number.zeroPad(date.getDate()) + '.' + number.zeroPad(date.getMonth() + 1) + '.' + date.getFullYear();
    };

    date.formatTime = function(date) {
        return number.zeroPad(date.getHours()) +':'+ number.zeroPad(date.getMinutes()) +':'+ number.zeroPad(date.getSeconds());
    };

    date.formatDateTime = function(dateParam) {
        return date.formatDate(dateParam) +' '+ date.formatTime(dateParam);
    };

    date.formatDuration = function(duration) {
        var hours = Math.floor(duration / 3600),
            minutes =  Math.floor((duration % 3600) / 60),
            seconds = duration % 60;

        return  (hours > 0 ? hours + ':' : '') +  number.zeroPad(minutes) + ':' + number.zeroPad(seconds);
    };

    return date;
});