define(function () {
    var config = {
        serverUrl: 'http://pagelivecharts.vgnett.no:8080/api/v1/',
        originUrl: 'http://pagelivecharts.vgnett.no:8080'
    };
    return config;
});