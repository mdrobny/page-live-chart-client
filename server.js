var express = require('express'),
    app = express(),
    config = require('./config'),
    serveStatic = require('serve-static'),
    domains = ['www.minmote.no', 'www.godt.no'];

app.set('view engine', 'ejs');
app.set('views', __dirname);

app.use(serveStatic(__dirname + '/public'));

var basicAuth = require('basic-auth');

var auth = function (req, res, next) {
    function unauthorized(res) {
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.sendStatus(401);
    };

    var user = basicAuth(req);

    if (!user || !user.name || !user.pass) {
        return unauthorized(res);
    };

    if (user.name === config.user && user.pass === config.password) {
        return next();
    } else {
        return unauthorized(res);
    };
};

app.get('/', function(req, res) {
    res.redirect('/' + domains[0]);
});

app.get('/:domain', auth, function(req, res) {
    if(domains.indexOf(req.params.domain) === -1) {
        res.redirect('/' + domains[0]);
    }
    else {
        res.render('index', {domain: req.params.domain, apiToken: config.apiToken});
    }
});

var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('App listening at http://%s:%s', host, port)
});