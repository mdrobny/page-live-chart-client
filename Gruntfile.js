module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    var port = 35730;
    grunt.config.init({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            html: {
                files: ['public/index.html', 'public/templates/**/*.html'],
                options: {
                    livereload: port
                }
            },
            js: {
                files: ['public/js/**/*.js'],
                options: {
                    livereload: port
                }
            },
            scss: {
                files: ['public/scss/**/*.scss'],
                tasks: ['compass']
            },
            css: {
                files: ['public/css/*.css'],
                options: {
                    livereload: port
                }
            }
        },
        compass: {
            dist: {
                options: {
                    sassDir: 'public/scss',
                    cssDir: 'public/css'
                }
            }
        },
        wiredep: {
            task: {
                src: [
                    'public/index.html',
                    'public/js/*.js'
                ]
            }
        }

    });

    grunt.registerTask('default', ['watch']);

};